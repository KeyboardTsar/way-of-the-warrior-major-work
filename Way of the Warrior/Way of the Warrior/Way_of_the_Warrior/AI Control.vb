﻿Public Class AI

    Inherits Character 'Sub class of character

    Public Sub updatePlayer(ByVal gameTime As GameTime) 'mainline

        ks = Keyboard.GetState
        checkForMovement(gameTime) 'this is the only function, but it is below so the mainline is clean
        destRect = New Rectangle(xLoc, yLoc, destRectWidth, 306)

    End Sub

    Public Sub checkForMovement(ByVal gameTime As GameTime) 'order of operations for AI i.e. which commands are checked first (death before attacking)
        If isDead = False Then
            If disabled = False Then
                If injured = False Then
                    If attacking = True Or jumpAttacking = True Then
                        If injured = False And isDead = False And jumpingInjured = False And crouchInjured = False Then
                            For z = 0 To 6
                                If booleansArr(z) = True Then
                                    GenericAttackFunction(gameTime, z)
                                End If
                            Next
                        End If
                    Else
                        If jumpingInjured = True Then
                            jumpAnimate(gameTime)
                        Else
                            If jumping = False Then
                                If stopMoving = False Then
                                    movingAnimate(gameTime)
                                Else
                                    idlingAnimate(gameTime)
                                End If
                            End If
                        End If
                    End If
                Else
                    If falling = True Then
                        injuredAnimate(gameTime, True)
                    Else
                        injuredAnimate(gameTime, False)
                    End If
                End If
            Else
                idlingAnimate(gameTime)
            End If
        Else
            dyingAnimate(gameTime)
        End If

    End Sub

End Class
