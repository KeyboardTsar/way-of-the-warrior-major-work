﻿Public Class Player

    Inherits Character 'Sub class of character

    Public Sub updatePlayer(ByVal gameTime As GameTime) 'mainline

        ks = Keyboard.GetState
        checkForMovement(gameTime)
        destRect = New Rectangle(xLoc, yLoc, destRectWidth, 306) ' must find a way so this is not at the end

    End Sub

    Public Sub checkForMovement(ByVal gameTime As GameTime) 'order of operations and checking for user key input
        If isDead = False Then
            If disabled = False Then
                If injured = False Then
                    If attacking = True Then 'this is for continuing current attacks
                        For z = 0 To 6
                            If booleansArr(z) = True Then
                                GenericAttackFunction(gameTime, z)
                            End If
                        Next
                    Else
                        For z = 0 To 3 'check if an attack is being initiated
                            If ks.IsKeyDown(keysArr(z)) And crouchInjured = False Then
                                If jumping = True Then
                                    If jumpingInjured = False Then 'jumping attacks are not animations, so are treated separately
                                        jumpAttacking = True
                                        If ks.IsKeyDown(Keys.A) Or ks.IsKeyDown(Keys.S) Then
                                            jumpPunching = True
                                        Else
                                            jumpPunching = False
                                        End If
                                    End If
                                Else
                                    attacking = True
                                    If crouching = True Then 'if crouching
                                        z += 4

                                    End If

                                    If z > 4 And ((z Mod 2) <> 0) Then
                                        z = z - 1 'this is because there are double ups in the array - the algorithm requires it
                                    End If

                                    booleansArr(z) = True
                                    GenericAttackFunction(gameTime, z)
                                End If
                            End If
                        Next

                        If jumping = False Then
                            If crouching = False Then 'if not crouching

                                If ks.IsKeyDown(Keys.Up) Then
                                    jumpAnimate(gameTime)
                                    jumping = True
                                ElseIf ks.IsKeyDown(Keys.Down) Then
                                    crouchAnimate(gameTime)
                                ElseIf ks.IsKeyDown(Keys.Right) Then
                                    goingRight = True
                                    movingAnimate(gameTime)
                                ElseIf ks.IsKeyDown(Keys.Left) Then
                                    goingRight = False
                                    movingAnimate(gameTime)
                                Else
                                    idlingAnimate(gameTime)
                                End If
                            Else 'if you are crouching
                                If ks.IsKeyUp(Keys.Down) Then 'If crouching but key isn't being pressed, get up
                                    crouching = False
                                    idlingAnimate(gameTime)
                                    destRectControl()
                                Else
                                    If crouchInjured = True Then
                                        sourceRect = New Rectangle(1350, 0, 49, 102)
                                    Else
                                        sourceRect = crouchRect 'This line necessary to finish crouch attacks
                                        destRectControl()
                                    End If
                                End If
                            End If
                        Else ' if you are jumping
                            jumpAnimate(gameTime)
                        End If
                    End If
                Else
                    If falling = True Then
                        injuredAnimate(gameTime, True)
                    Else
                        injuredAnimate(gameTime, False)
                    End If
                End If
            Else
                idlingAnimate(gameTime)
            End If
        Else
            dyingAnimate(gameTime)
        End If


    End Sub

End Class
