﻿Public Class Button

    'The variables below are never changed from outside the 

    Public buttonTexture As Texture2D
    Public buttonPosition As Vector2
    Public buttonRect As Rectangle
    Public buttonColour As Color = New Color(255, 255, 255, 255)
    Public buttonSize As Vector2
    Public down As Boolean

    Public Overridable Sub Button(newTexture As Texture2D) 'Constructor

        buttonTexture = newTexture
        buttonSize = New Vector2(298, 75)

    End Sub

    Public Sub Update(selected As Boolean) 'Mainline

        buttonRect = New Rectangle(buttonPosition.X, buttonPosition.Y, buttonSize.X, buttonSize.Y)

        changeButtonColor(selected)

    End Sub

    Public Sub changeButtonColor(selected As Boolean) 'highlights the button to show it is selected
        If selected = True Then
            If buttonColour.A = 255 Then
                down = False
            ElseIf buttonColour.A = 0 Then
                down = True
            End If

            If down = True Then
                buttonColour.A += 5
            Else
                buttonColour.A -= 5
            End If

        ElseIf buttonColour.A < 255 Then
            buttonColour.A += 5
        End If
    End Sub

    Public Sub setPosition(newPosition As Vector2) 'sets button position on screen
        buttonPosition = newPosition
    End Sub

    Public Sub draw(spriteBatch As SpriteBatch) 'renders button
        spriteBatch.Draw(buttonTexture, buttonRect, buttonColour)
    End Sub

End Class
