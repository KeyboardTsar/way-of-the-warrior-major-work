
'Mr Howse: you will not that this class (the main class) accesses attributes from objects. Please
'note that it never changes an object's attributes, only checks it. I have this conforms to 
'encapsulation.

Public Class Game1

    Inherits Microsoft.Xna.Framework.Game

    Private WithEvents graphics As GraphicsDeviceManager
    Private WithEvents spriteBatch As SpriteBatch

    Private fighting As Boolean = False

    Private ks As KeyboardState

    Private gameOver As Boolean = False
    Private menuButton As New Button
    Private continueButton As New Button
    Private gameOverOptions As Array = {continueButton, menuButton}
    Private GOButtonSelected As Array = {True, False}
    Private GObuttonTexts As Array = {"Continue", "Menu"}

    Private elapsed As Double
    Private canAttack As Boolean = True
    Private attackCounter As Integer = 0
    Private moveElapsed As Double
    Private time As Double = 99
    Private timeCounter As Double = 0
    Private buttonfont As SpriteFont
    Private msgfont As SpriteFont
    Private playerWins As Boolean
    Private doubleKO As Boolean
    Private levelNum As Integer = 1
    Private tempString As String
    Private gameStarted As Boolean = False
    Private chosenChar As Integer

    Private MenuTexture As Texture2D
    Private helpText As Texture2D
    Private MsgBox As Texture2D
    Private characterTexture As Texture2D
    Private AITexture As Texture2D
    Private RyuTexture As Texture2D
    Private KenTexture As Texture2D
    Private FeiTexture As Texture2D
    Private textureArray As Array = {"RyuSheet", "KenSheet", "FeiSheet"}
    Private tempTexture As Texture2D
    Private HitMarker As Texture2D
    Private HealthBar As Texture2D
    Private PlayerBar As Texture2D
    Private AIBar As Texture2D
    Private JapanBackgroundTexture As Texture2D
    Private AFBackground As Texture2D
    Private abstractBackground As Texture2D
    Private BGArray As Array = {JapanBackgroundTexture, AFBackground, abstractBackground}
    Private currentBG As Integer

    Private showMarker As Boolean = False
    Private markerDestRect As Rectangle

    Private randomInt As Integer
    Private random As New Random

    Private Menu As New MainMenu
    Private pc As New Player 'the player
    Private aic As New AI 'the AI
    Private classArray As Array = {pc, aic, pc}

    Private playerDirection As SpriteEffects = SpriteEffects.None
    Private aiDirection As SpriteEffects = SpriteEffects.FlipHorizontally

    Private gameTime As GameTime

    Public Sub New()
        graphics = New GraphicsDeviceManager(Me)
        Content.RootDirectory = "Content"
    End Sub

    Protected Overrides Sub Initialize()

        tempTexture = Content.Load(Of Texture2D)("button")

        For i = 0 To 1
            gameOverOptions(i).Button(tempTexture)
            gameOverOptions(i).setPosition(New Vector2(254, 180 + (i * 90)))
        Next

        MsgBox = Content.Load(Of Texture2D)("MsgBox")
        helpText = Content.Load(Of Texture2D)("HelpText")
        RyuTexture = Content.Load(Of Texture2D)("RyuSheet")
        KenTexture = Content.Load(Of Texture2D)("KenSheet")
        FeiTexture = Content.Load(Of Texture2D)("FeiSheet")
        HitMarker = Content.Load(Of Texture2D)("Hitmarker")
        HealthBar = Content.Load(Of Texture2D)("Health Bar")
        PlayerBar = Content.Load(Of Texture2D)("Barcolour")
        AIBar = Content.Load(Of Texture2D)("Barcolour")
        buttonfont = Content.Load(Of SpriteFont)("TimerFont")
        msgfont = Content.Load(Of SpriteFont)("MsgFont")
        MenuTexture = Content.Load(Of Texture2D)("MainMenuBG")

        'Main Menu constructor:
        Menu.MainMenu(helpText, buttonfont, msgfont, MenuTexture, tempTexture, RyuTexture, KenTexture, FeiTexture, MsgBox)

        MyBase.Initialize()

    End Sub

    Protected Overrides Sub LoadContent()

        spriteBatch = New SpriteBatch(GraphicsDevice)

        'Screen stuff:
        Me.IsMouseVisible = False
        graphics.PreferredBackBufferWidth = 800
        graphics.PreferredBackBufferHeight = 480
        graphics.ApplyChanges()

    End Sub

    Protected Overrides Sub Update(ByVal gameTime As GameTime) 'Mainline

        ks = Keyboard.GetState

        If fighting = True Then
            combatMainLine(gameTime) 'Combat Mainline
        Else
            Menu.Update() 'Menu Mainline
        End If

        ChangeGameState()

        MyBase.Update(gameTime)

    End Sub

    Public Sub ChangeGameState() 'Can switch to combat or exit the game

        If Menu.fighting = True Then
            If gameStarted = False Then
                resetGame()
            End If
            fighting = True
        ElseIf Menu.kill = True Then
            Me.Exit()
        End If

    End Sub

    Public Sub combatMainLine(ByVal gameTime As GameTime) 'calls all combat functions

        If gameOver = False Then
            updateTimer(gameTime)
            initialiseAiAttacks(gameTime)
            checkForDeath() 'handles players after game ends
            determineOutcome() 'handles screen elements after game ends
            determineDirections()
            manageHealth()
            checkForSuccessfulAttack(gameTime)
        Else
            MsgBoxControl()
            handleMsgBox()
        End If

        For i = 0 To 1 'update both players and both buttons
            classArray(i).updatePlayer(gameTime)
            gameOverOptions(i).Update(GOButtonSelected(i))
        Next

    End Sub

    Public Sub resetGame() 'resets main values and creates new object

        gameOver = False
        chosenChar = Menu.ChosenChar
        characterTexture = Content.Load(Of Texture2D)(textureArray(chosenChar))
        elapsed = 0
        canAttack = True
        attackCounter = 0
        moveElapsed = 0
        time = 99
        timeCounter = 0
        pc = New Player 'new player
        aic = New AI 'new AI
        pc.Character(True, 100)
        aic.Character(False, 550)
        classArray = {pc, aic, pc}
        chooseBG()
        chooseAI()
        gameStarted = True

    End Sub

    Public Sub chooseBG() 'chooses one of three random backgrounds

        JapanBackgroundTexture = Content.Load(Of Texture2D)("JapanBackground")
        AFBackground = Content.Load(Of Texture2D)("AFBackground")
        abstractBackground = Content.Load(Of Texture2D)("AbstractBackground")
        BGArray = {JapanBackgroundTexture, AFBackground, abstractBackground} 'This must be redifined each reset to be drawn 
        randomInt = random.Next(0, 3)
        currentBG = randomInt

    End Sub

    Public Sub chooseAI() 'randomly selects one of the two characters the player hasn't picked

        randomInt = random.Next(0, 3)
        While randomInt = chosenChar
            randomInt = random.Next(0, 3)
        End While
        AITexture = Content.Load(Of Texture2D)(textureArray(randomInt))

    End Sub

    Public Sub handleMsgBox() 'handles all message box commands e.g. go back to menu

        If gameOver = True Then
            If ks.IsKeyDown(Keys.Enter) Or ks.IsKeyDown(Keys.Space) Then
                resetGame()
                gameStarted = False
                If GOButtonSelected(1) = True And GOButtonSelected(0) = False Then 'Next Level
                    levelNum = 1
                    Menu.ResetMenu()
                    fighting = False
                ElseIf GOButtonSelected(0) = True And GOButtonSelected(1) = False Then 'Back to Menu
                    levelNum += 1
                End If
            End If
        End If

    End Sub

    Public Sub determineOutcome() 'determines the outcome of the fight e.g. double KO 

        If aic.isDead = True And pc.isDead = True Then 'double knockout
            gameOver = True
            playerWins = False
            doubleKO = True
        Else
            doubleKO = False
            If aic.isDead = True Or pc.isDead = True Or time <= 0 Then
                gameOver = True
                If aic.isDead = True Then
                    playerWins = True
                ElseIf pc.isDead = True Then
                    playerWins = False
                ElseIf time <= 0 And gameOver = False Then
                    If pc.health > aic.health Then
                        playerWins = True
                    Else
                        playerWins = False
                    End If
                End If
            End If
        End If

    End Sub

    Public Sub manageHealth() 'calls object commands for incrementing health
        For i = 0 To 1
            classArray(i).incrementHealth()
        Next
    End Sub

    Private Sub updateTimer(ByVal gameTime As GameTime) 'handles timer operations i.e. counts down

        timeCounter += gameTime.ElapsedGameTime.TotalMilliseconds
        If timeCounter > 2000 Then
            If time > 0 Then
                time -= 1
            End If
            timeCounter = 0
        Else
            timeCounter += gameTime.ElapsedGameTime.TotalMilliseconds
        End If

    End Sub

    Private Sub determineDirections() 'this is to determine which player faces which directione
        For i = 0 To 1
            If pc.xLoc < aic.xLoc Then
                playerDirection = SpriteEffects.None
                aiDirection = SpriteEffects.FlipHorizontally
                pc.determineMyDirection(True)
                aic.determineMyDirection(False)
            Else
                playerDirection = SpriteEffects.FlipHorizontally
                aiDirection = SpriteEffects.None
                pc.determineMyDirection(False)
                aic.determineMyDirection(True)
            End If
        Next
    End Sub

    Private Sub AnimateMarker(ByVal gameTime As GameTime, x As Integer, y As Integer) 'this sub animates the hit maker

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds

        markerDestRect = New Rectangle(x, y, 32, 32)

        If elapsed > 50 Then
            showMarker = False
            elapsed = 0
        End If

    End Sub

    Private Sub MsgBoxControl() 'determines which message box to show

        If gameOver = True And playerWins = True Then
            If ks.IsKeyDown(Keys.Down) And GOButtonSelected(1) = False Then
                For i = 0 To 1
                    GOButtonSelected(i) = False
                Next
                GOButtonSelected(1) = True
            ElseIf ks.IsKeyDown(Keys.Up) And GOButtonSelected(0) = False Then
                For i = 0 To 1
                    GOButtonSelected(i) = False
                Next
                GOButtonSelected(0) = True
            End If
        End If

    End Sub

    Private Sub checkForSuccessfulAttack(ByVal gameTime As GameTime) 'check for hits that are in range and in the right stance

        If aic.isDead = False And pc.isDead = False Then
            For i = 0 To 1

                If ((classArray(i).attacking = True And classArray(i).attackFrame = 1) Or (classArray(i).jumpAttacking = True And classArray(i + 1).crouching = False) Or (classArray(i).crouching = True And classArray(i).attacking = True And classArray(i + 1).jumping = False)) Then 'Three scenarios: standing attack hurts all, crouch attack hurts
                    If (classArray(i).xLoc > classArray(i + 1).xLoc And classArray(i).xLoc < (classArray(i + 1).xLoc + 130)) Or (classArray(i).xLoc < classArray(i + 1).xLoc And classArray(i).xLoc > (classArray(i + 1).xLoc - 130)) Then 'If player in range                              crouch and standing, jumping hurts standing and jumping

                        showMarker = True

                        If classArray(i).jumping = True And classArray(i + 1).crouching = False Then

                            If classArray(i + 1).jumping = True Then 'jumper attacks jumper
                                attackScenario(gameTime, i, 8, 0, 50, 205)
                            Else 'jumper attacks standing
                                attackScenario(gameTime, i, 0, 0, 50, 60)
                            End If

                        ElseIf classArray(i).crouching = True And classArray(i + 1).jumping = False Then

                            If classArray(i + 1).crouching = True Then 'croucher attacks crouchher
                                attackScenario(gameTime, i, 10, 1, 50, 180)
                            Else 'croucher attacks standing
                                attackScenario(gameTime, i, 3, 0, 50, 205)
                            End If

                        ElseIf classArray(i).jumping = False And classArray(i).crouching = False Then

                            If classArray(i + 1).jumping = True Then
                                attackScenario(gameTime, i, 8, 0, 50, 205)
                            ElseIf classArray(i + 1).crouching = True Then
                                attackScenario(gameTime, i, 10, 1, 50, 180)
                            Else
                                attackScenario(gameTime, i, 0, 0, 50, 60)
                            End If
                        End If

                        If (classArray(i + 1).jumping = True And classArray(i).crouching = True) = False Then
                            If classArray(i + 1).health < 4 Then 'death
                                classArray(i + 1).passingAttributes(4, 1)
                                classArray(i).passingAttributes(5, 1)
                                showMarker = False
                            Else 'subtract health
                                classArray(i + 1).passingAttributes(2, 1)
                            End If

                        End If
                    End If
                End If

                If classArray(i).Attacking = False Then
                    classArray(i + 1).passingAttributes(10, 0)
                End If
            Next

            If aic.attacking = False And aic.jumpAttacking = False And pc.attacking = False And pc.jumpAttacking = False Then
                showMarker = False
            End If

        End If

    End Sub

    'A generic bit of code used in the subroutine above:
    Public Sub attackScenario(ByVal gameTime As GameTime, ID As Integer, parameter1 As Integer, parameter2 As Integer, x As Integer, y As Integer)

        classArray(ID + 1).passingAttributes(parameter1, parameter2)
        AnimateMarker(gameTime, classArray(ID + 1).xLoc + x, classArray(ID + 1).yLoc + y)

    End Sub

    Private Sub createPause(ByVal gameTime As GameTime) 'create and change a pause between AI attacks depending on difficulty 

        moveElapsed += gameTime.ElapsedGameTime.TotalMilliseconds
        If (800 - (70 * levelNum)) > 100 Then 'gets harder until level 10
            If moveElapsed > (800 - (75 * levelNum)) Then
                moveElapsed = 0
                canAttack = True
            End If
        Else
            If moveElapsed > 100 Then
                moveElapsed = 0
                canAttack = True
            End If
        End If

    End Sub

    Private Sub checkForDeath() 'handles player actions when they die

        For i = 0 To 1
            If classArray(i).health < 4 Then 'death
                classArray(i).passingAttributes(4, 1)
                classArray(i + 1).passingAttributes(5, 1)
                showMarker = False
            End If
        Next

    End Sub

    Private Sub initialiseAiAttacks(ByVal gameTime As GameTime) 'algorithm for AI behaviour

        If aic.injured = False And aic.disabled = False And aic.falling = False And pc.falling = False And aic.jumpingInjured = False Then
            If aic.jumping = False Then
                If aic.attacking = False Then
                    If (aic.xLoc >= pc.xLoc And aic.xLoc <= (pc.xLoc + 110)) Or (aic.xLoc <= pc.xLoc And aic.xLoc >= (pc.xLoc - 110)) Then

                        aic.passingAttributes(11, 1)

                        If canAttack = True Then
                            attackCounter += 1
                            If attackCounter = 2 Then
                                canAttack = False
                                attackCounter = 0
                            End If
                            randomInt = random.Next(0, 4) '0, 4
                            If randomInt = 2 Then
                                randomInt = random.Next(4, 6)
                                If randomInt = 5 Then
                                    randomInt += 1
                                End If

                                aic.passingAttributes(7, randomInt)
                                aic.GenericAttackFunction(gameTime, randomInt)
                            ElseIf randomInt = 3 Then
                                randomInt = random.Next(0, 2)
                                aic.passingAttributes(6, randomInt)
                                aic.jumpAnimate(gameTime)
                            Else
                                randomInt = random.Next(0, 4)
                                aic.passingAttributes(1, randomInt)
                                aic.GenericAttackFunction(gameTime, randomInt)
                            End If
                        Else
                            createPause(gameTime)
                        End If

                    Else
                        aic.idlingAnimate(aic.gameTime) 'move
                        aic.passingAttributes(11, 0)
                    End If

                Else

                    For z = 0 To 6
                        If aic.booleansArr(z) = True Then
                            aic.GenericAttackFunction(aic.gameTime, z)
                        End If
                    Next
                End If

            Else

                If aic.yLoc < 50 And aic.jumpAttacking = False Then
                    randomInt = random.Next(0, 2)
                    aic.passingAttributes(9, randomInt)
                End If
                aic.jumpAnimate(gameTime)
            End If
        End If

    End Sub

    Public Sub gameOverDrawing() 'what to draw after the game ends

        spriteBatch.Draw(MsgBox, New Rectangle(243, 125, 320, 230), Color.White)
        gameOverOptions(1).draw(spriteBatch) 'always able to go to menu
        spriteBatch.DrawString(buttonfont, GObuttonTexts(1), New Vector2(356, 287), Color.White)

        If playerWins = True Then
            tempString = "You Win Level" + Str(levelNum) + "!"
            spriteBatch.DrawString(msgfont, tempString, New Vector2(325, 143), Color.Black)
            gameOverOptions(0).draw(spriteBatch)
            spriteBatch.DrawString(buttonfont, GObuttonTexts(0), New Vector2(325, 195), Color.White)
        Else
            GOButtonSelected(0) = False
            GOButtonSelected(1) = True
            If doubleKO = True Then
                tempString = "Double KO!"
            Else
                tempString = "You Lose!"
            End If
            spriteBatch.DrawString(msgfont, tempString, New Vector2(348, 150), Color.Black)
            tempString = "You Reached Level" + Str(levelNum) + "."
            spriteBatch.DrawString(msgfont, tempString, New Vector2(300, 205), Color.Black)
        End If

    End Sub

    Protected Overrides Sub Draw(ByVal gameTime As GameTime) 'render all screen elements
        GraphicsDevice.Clear(Color.CornflowerBlue)

        spriteBatch.Begin()
        If fighting = True Then
            If gameStarted = True Then
                spriteBatch.Draw(BGArray(currentBG), New Rectangle(0, 0, 800, 480), Color.White)
                spriteBatch.Draw(characterTexture, pc.destRect, pc.sourceRect, Color.White, 0, New Vector2(0, 0), playerDirection, 0) 'Can also be Ken or Fei
                spriteBatch.Draw(AITexture, aic.destRect, aic.sourceRect, Color.White, 0, New Vector2(0, 0), aiDirection, 0)
                spriteBatch.Draw(PlayerBar, New Rectangle(112 + 266 * (1 - (pc.health / 100)), 32, (250 * (pc.health / 100)), 40), Color.White)
                spriteBatch.Draw(AIBar, New Rectangle(429, 32, (266 * (aic.health / 100)), 40), Color.White)
                spriteBatch.Draw(HealthBar, New Rectangle(100, 20, 600, 60), Color.White)
                spriteBatch.DrawString(buttonfont, CStr(time), New Vector2(377, 28), Color.DeepSkyBlue)
                If showMarker = True Then
                    spriteBatch.Draw(HitMarker, markerDestRect, Color.White)
                End If

                If gameOver = True Then
                    gameOverDrawing()
                End If
            End If
        Else

            Menu.draw(spriteBatch)

        End If
        spriteBatch.End()

        MyBase.Draw(gameTime)
    End Sub

End Class