﻿Public Class Character

    Inherits Microsoft.Xna.Framework.Game

    'PLEASE NOTE: Most variables are public only so that they can be accessed
    'by subclasses. They are read by other classes but never changed. 
    'Encapsulation is still observed.

    Private WithEvents graphics As GraphicsDeviceManager
    Private WithEvents spriteBatch As SpriteBatch

    Public ks As KeyboardState

    Public destRect As Rectangle
    Public destRectWidth As Integer = 150
    Public sourceRect As Rectangle
    Public crouchRect As Rectangle = New Rectangle(1159, 0, 49, 102)

    Public gameTime As New GameTime

    Public elapsed As Double = 0
    Public crouchElapsed As Double = 0
    Public delay As Double = 100 'animation speed for moving 100
    Public attackDelay As Double = 140 'animation speed for attacking 140

    Public frameNum As Integer = 0 'used for idling and stepping
    Public attackFrame As Integer = 0 'used for attacking and jumping

    Public jumpTime As Integer = 1
    Public jumping As Boolean = False
    Public jumpingInjured As Boolean = False
    Public jumpAttacking As Boolean = False
    Public jumpPunching As Boolean = True 'false is jump kicking

    Public crouching As Boolean = False
    Public crouchInjured As Boolean = False
    Public attacking As Boolean = False
    Public injured As Boolean = False
    Public falling As Boolean = False
    Public isDead As Boolean = False
    Public disabled As Boolean = False
    Public stopMoving As Boolean = False

    Public health As Double = 100

    'The five arrays below are all needed for the generic attack function
    'The order is: jab, cross, front kick, back kick, crouch punch, nul, crouch kick
    'The zero values are skipped

    Private frameWidthArr As Integer() = {60, 75, 70, 92, 66, 0, 75} 'how wide the rectangle must be
    Private xCoOrds As Integer() = {0, 230, 0, 985, 178, 0, 695} 'how far along the spirte sheet the sprite is
    Private yCoOrds As Integer() = {116, 116, 247, 247, 375, 0, 375} 'how far down the sprite sheet the sprite is
    Private destRectWidthArr As Integer() = {180, 230, 230, 270, 200, 0, 220} 'the width the destRect must be changed to
    Public booleansArr As Boolean() = {False, False, False, False, False, False, False} 'the boolean for the attack


    Public keysArr As Keys() = {Keys.A, Keys.S, Keys.Z, Keys.X}

    Public goingRight As Boolean

    Public xLoc As Integer
    Public yLoc As Integer = 150

    Public Sub Character(goingRightTemp As Boolean, xLocTemp As Integer) 'Constructor

        goingRight = goingRightTemp
        xLoc = xLocTemp

    End Sub

    Public Sub determineMyDirection(tempGoingRight As Boolean) 'determine which way the player faces

        goingRight = tempGoingRight

    End Sub

    Public Sub destRectControl() 'this is for determining what size the frame goes back to

        If jumping = True Then
            If jumpAttacking = True Then
                If jumpPunching = True Then
                    destRectWidth = 170
                Else
                    destRectWidth = 200
                End If
            ElseIf jumpingInjured = True Then
                destRectWidth = 150
            Else
                destRectWidth = 130
            End If
        Else 'crouching, idling and moving
            destRectWidth = 150
        End If

    End Sub

    Public Sub basicFrameCycle(max As Integer) 'this is for moving and idling, where the frame must reset

        If elapsed > delay Then
            If frameNum >= max Then
                frameNum = 0
            Else
                frameNum += 1
            End If
            elapsed = 0
        End If

    End Sub

    Public Sub fallingAnimate() 'when the character falls

        destRectWidth = 200

        basicFrameCycle(8)
        sourceRect = New Rectangle((74 * frameNum) + 904, 747, 74, 102)

        If frameNum = 8 Then
            injured = False
            falling = False
        End If

    End Sub

    Public Sub dyingAnimate(ByVal gameTime As GameTime) 'when the character dies

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds
        destRectWidth = 200
        basicAttackCycle(3)
        sourceRect = New Rectangle((74 * attackFrame) + 904, 747, 74, 102)

    End Sub

    Public Sub basicAttackCycle(max As Integer) 'basic frame cycle used for all attacks, jumping and injury

        If elapsed > attackDelay Then
            If attackFrame < max Then
                attackFrame += 1
            End If
            elapsed = 0
        End If

    End Sub

    Public Sub idlingAnimate(ByVal gameTime As GameTime) 'for when the character is idling

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds

        destRectControl()
        basicFrameCycle(3)

        sourceRect = New Rectangle((49 * frameNum) + 2, 0, 49, 102)

    End Sub

    Public Sub movingAnimate(ByVal gameTime As GameTime) 'for when the character is moving right or left

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds

        basicFrameCycle(4)

        basicMovementAlgorithm()

        sourceRect = New Rectangle((49 * frameNum) + 200, 0, 49, 102)

    End Sub

    Public Sub basicMovementAlgorithm() 'moves the frame and checks if the boundaries have been reached

        If goingRight = True Then
            If xLoc < 657 Then
                xLoc += 5
            End If
        Else
            If xLoc > 0 Then
                xLoc -= 5
            End If
        End If

    End Sub

    Public Sub incrementHealth() 'adds health over time and subtracts health lost by attacking

        If disabled = False And isDead = False And injured = False And falling = False Then
            If attacking = False And jumpAttacking = False And health < 100 Then
                health += 0.1
            Else
                If jumpAttacking = True Then
                    health -= 0.6
                Else
                    health -= 0.2
                End If
            End If
        End If

    End Sub

    Public Sub passingAttributes(a As Integer, b As Integer) 'this is for encapsulation. Attributes
        '                                                    'are passed by other classes and changed here.

        If a = 0 Then 'character injured
            injured = True
        ElseIf a = 1 Then 'initiate a certain attack
            crouching = False
            attacking = True
            booleansArr(b) = True
        ElseIf a = 2 Then 'subtract from health due to damage
            health -= b
        ElseIf a = 3 Then 'player falling
            injured = True
            falling = True
        ElseIf a = 4 Then 'player dying
            attackFrame = 0
            isDead = True
            yLoc = 150
        ElseIf a = 5 Then 'player disabled
            disabled = True
            jumping = False
            yLoc = 150
        ElseIf a = 6 Then 'player jumping
            crouching = False
            jumping = True
            attacking = False
            destRectControl()
        ElseIf a = 7 Then 'crouch attacking
            attacking = True
            crouching = True
            booleansArr(b) = True
        ElseIf a = 8 Then 'injured while jumping
            jumpAttacking = False
            jumpingInjured = True
            destRectWidth = 150
        ElseIf a = 9 Then 'attacking while jumping
            jumpAttacking = True
            jumpPunching = b 'Converting integer to boolean
        ElseIf a = 10 Then 'injured while crouching
            crouchInjured = b
            If b = True Then
                attacking = False
            End If
        ElseIf a = 11 Then 'player must stop moving
            stopMoving = b
        End If

    End Sub

    Public Sub injuredAnimate(ByVal gameTime As GameTime, fallingTemp As Boolean) 'for when the character is injured. Checks if character must fall.

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds
        If attacking = True Then 'interrupts attacks
            attackFrame = 0
            attacking = False
            For z = 0 To 6
                If booleansArr(z) = True Then
                    booleansArr(z) = False
                End If
            Next
        End If

        If injured = False Then
            frameNum = 0
        End If

        injured = True

        If fallingTemp = False Then
            falling = False
            destRectWidth = 180

            basicFrameCycle(2)
            sourceRect = New Rectangle((55 * frameNum) + 430, 747, 55, 102)

            If frameNum = 2 Then
                injured = False
            End If
        Else
            falling = True
            fallingAnimate()
        End If

    End Sub

    Public Sub crouchAnimate(ByVal gameTime As GameTime) 'for when the character is crouching

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds

        If crouchInjured = True Then
            sourceRect = New Rectangle(1350, 0, 49, 102)
        Else
            If sourceRect <> crouchRect Then 'This is to check if the crouch is beginning
                sourceRect = New Rectangle(1110, 0, 49, 102) 'This is the halfway frame
            End If

            If elapsed > delay Then
                crouching = True
                destRectControl()
                sourceRect = crouchRect
                elapsed = 0
            End If
        End If

    End Sub

    Private Sub jumpingMovement() ' Frame going up and down

        If jumpTime <= 18 Then 'jump upwards until a certain point
            yLoc -= 10
        Else 'return downwards
            yLoc += 10
            If jumpTime = 36 Then 'stop once the sprite has returned to the ground
                jumping = False
                destRectControl()
                jumpAttacking = False
                jumpingInjured = False
                attackFrame = 0
                jumpTime = 0
            End If
        End If

    End Sub

    Public Sub jumpAnimate(ByVal gameTime As GameTime) 'main jumping sub - jump animation and jump attacks

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds

        destRectWidth = 130

        jumpingMovement()

        jumpTime += 1

        If ks.IsKeyDown(Keys.Right) Then
            goingRight = True
            basicMovementAlgorithm()
        ElseIf ks.IsKeyDown(Keys.Left) Then
            goingRight = False
            basicMovementAlgorithm()
        End If

        If jumpAttacking = True Then
            If jumpPunching = True Then
                sourceRect = New Rectangle(44, 540, 55, 102)
                destRectWidth = 170
            Else
                sourceRect = New Rectangle(530, 550, 70, 102)
                destRectWidth = 200
            End If
        ElseIf jumpingInjured = True Then
            sourceRect = New Rectangle(840, 0, 45, 102)
            destRectWidth = 150
        Else 'animation only happens if not attacking
            basicAttackCycle(4) 'jump uses this as well
            sourceRect = New Rectangle((40 * attackFrame) + 490, 0, 40, 102)
        End If

    End Sub

    Public Sub GenericAttackFunction(ByVal gameTime As GameTime, i As Integer) 'handles all ground attacks

        elapsed += gameTime.ElapsedGameTime.TotalMilliseconds

        basicAttackCycle(3)

        sourceRect = New Rectangle((frameWidthArr(i) * attackFrame) + xCoOrds(i), yCoOrds(i), frameWidthArr(i), 102)

        destRectWidth = destRectWidthArr(i)

        'Variables: width, xCoOrd, yCoOrd, destRectWidth, boolean

        If attackFrame = 3 Then
            destRectControl()
            attacking = False
            booleansArr(i) = False
            sourceRect = New Rectangle((frameWidthArr(i) * attackFrame) + xCoOrds(i), yCoOrds(i), frameWidthArr(i), 102)
            attackFrame = 0
        End If

    End Sub

End Class
