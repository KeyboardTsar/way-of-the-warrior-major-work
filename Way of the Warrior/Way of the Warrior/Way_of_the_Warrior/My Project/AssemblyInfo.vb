﻿Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
<Assembly: AssemblyTitle("Way_of_the_Warrior")>
<Assembly: AssemblyProduct("Way_of_the_Warrior")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyCopyright("Copyright ©  2015")>
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components.  If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type. Only Windows
' assemblies support COM.
<Assembly: ComVisible(False)>

' On Windows, the following GUID is for the ID of the typelib if this
' project is exposed to COM. On other platforms, it unique identifies the
' title storage container when deploying this assembly to the device.
<Assembly: Guid("d2d76cd8-d8f1-4b35-a082-877082c0f2fb")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
<Assembly: AssemblyVersion("1.0.0.0")>