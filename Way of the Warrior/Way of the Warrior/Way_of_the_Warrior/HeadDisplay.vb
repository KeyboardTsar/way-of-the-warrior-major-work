﻿Public Class HeadDisplay

    Inherits Button 'Sub class of button

    Private headTexture As Texture2D

    Public Overrides Sub Button(newTexture As Texture2D) 'graphics As GraphicsDevice)

        headTexture = newTexture
        buttonSize = New Vector2(100, 100)

    End Sub

    Public Sub draw(spriteBatch As SpriteBatch, DontBlackOut As Boolean) 'POLYMORPHISM - heads get passed one more parameter than buttons 
        '                                                                 and the game chooses which version of draw to execute at runtime

        If DontBlackOut = True Then
            spriteBatch.Draw(headTexture, buttonRect, New Rectangle(1420, 860, 100, 100), buttonColour)
        Else
            spriteBatch.Draw(headTexture, buttonRect, New Rectangle(1420, 860, 100, 100), Color.Black)
        End If

    End Sub

End Class
