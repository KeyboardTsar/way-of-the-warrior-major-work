﻿Public Class MainMenu

    Private ks As KeyboardState
    Private playButton As New Button
    Private helpButton As New Button
    Private exitButton As New Button
    Private buttonArray As Array = {playButton, helpButton, exitButton}
    Private buttonPos As Array = {165, 250, 340} 'FIX

    Private canSelect As Boolean = True
    Private MenuTexture As Texture2D
    Private buttCount As Integer = 0
    Private buttonDown As Array = {True, False, False}
    Private buttonTexts As Array = {"Play", "Help", "Exit"}
    Private helpVis As Boolean = False

    Private choosingChar As Boolean = False
    Private MsgBox As Texture2D
    Private helpText As Texture2D
    Private head1 As New HeadDisplay
    Private head2 As New HeadDisplay
    Private head3 As New HeadDisplay
    Private headArray As Array = {head1, head2, head3}
    Private headText1 As Array = {"Ryu is a ruthless Karate", "Ken is an brutal Jiu Jitsu", "Fei is a devastating Kung"}
    Private headText2 As Array = {" fighter from Japan", "fighter from the USA", "Fu fighter from China"}
    Private headSelected As Array = {True, False, False}
    Private headCount As Integer = 0
    Private backSelected As Boolean = False
    Private characterTexture As Texture2D

    Public fighting As Boolean = False
    Public ChosenChar As Integer = 0
    Public kill As Boolean = False

    Private Buttonfont As SpriteFont
    Private msgfont As SpriteFont
    Private textureArray As Array = {"RyuSheet", "KenSheet", "FeiSheet"}

    Public Sub MainMenu(helpText1 As Texture2D, font1 As SpriteFont, font2 As SpriteFont, MenuText As Texture2D, ButtonTexture As Texture2D, ryuTexture As Texture2D, kenTexture As Texture2D, feiTexture As Texture2D, helpDisplay1 As Texture2D)

        'This is a constructor 

        helpText = helpText1
        MsgBox = helpDisplay1
        Buttonfont = font1
        msgfont = font2
        MenuTexture = MenuText

        'This was too inefficient to do in a loop, as another array would have to be made:
        headArray(0).Button(ryuTexture)
        headArray(1).Button(kenTexture)
        headArray(2).Button(feiTexture)

        For i = 0 To 2
            buttonArray(i).Button(ButtonTexture) ', graphics.GraphicsDevice)
            buttonArray(i).setPosition(New Vector2(254, buttonPos(i)))
            headArray(i).setPosition(New Vector2(270 + (93 * i), 175))
        Next
    End Sub

    Public Sub Update() 'mainline

        ks = Keyboard.GetState

        checkForCommands() 'keeping mainline clear

    End Sub

    Public Sub ResetMenu() 'reset the menu and its settings

        For i = 0 To 2
            buttonDown(i) = False
            headSelected(i) = False
        Next

        buttonTexts(2) = "Exit"
        fighting = False
        buttCount = 0
        buttonDown(0) = True
        headSelected(0) = True
        helpVis = False
        backSelected = False
        choosingChar = False
        canSelect = False

    End Sub

    Public Sub checkForCommands()

        If ((ks.IsKeyDown(Keys.Space) = True) Or (ks.IsKeyDown(Keys.Enter) = True)) And canSelect = True Then

            If choosingChar = True And backSelected = False Then 'player has been picked

                For i = 0 To 2
                    If headSelected(i) = True Then
                        ChosenChar = i
                    End If
                Next

                fighting = True

            Else

                If helpVis = False Then
                    If buttonDown(0) = True Then

                        choosingChar = True
                        headCount = 0
                        headSelected(0) = True
                        backSelected = False
                        selectHelpOrChoosePlayer()

                    ElseIf buttonDown(1) = True Then

                        selectHelpOrChoosePlayer()
                        buttonDown(2) = True

                    ElseIf buttonDown(2) = True Then

                        kill = True

                    End If
                Else
                    If canSelect = True Then
                        canSelect = False
                        helpVis = False
                        choosingChar = False
                        buttonTexts(2) = "Exit"
                        buttCount = 1
                        For i = 0 To 2
                            buttonDown(i) = False
                        Next
                        buttonDown(1) = True
                    End If
                End If
            End If
        ElseIf ks.IsKeyDown(Keys.Down) And canSelect = True Then

            moveUpOrDown(False, True, 2)

        ElseIf ks.IsKeyDown(Keys.Up) And canSelect = True Then

            moveUpOrDown(True, False, 0)

        ElseIf ks.IsKeyDown(Keys.Left) And (headSelected(0) = False) And canSelect = True And choosingChar = True And backSelected = False Then

            moveRightOrLeft(False)

        ElseIf ks.IsKeyDown(Keys.Right) And (headSelected(2) = False) And canSelect = True And choosingChar = True And backSelected = False Then

            moveRightOrLeft(True)

        ElseIf ks.IsKeyUp(Keys.Up) And ks.IsKeyUp(Keys.Down) And ks.IsKeyUp(Keys.Space) And ks.IsKeyUp(Keys.Enter) And ks.IsKeyUp(Keys.Right) And ks.IsKeyUp(Keys.Left) Then
            canSelect = True 'only single movements
        End If

        For i = 0 To 2
            buttonArray(i).Update(buttonDown(i))
            headArray(i).Update(headSelected(i))
        Next

    End Sub

    'Common bit of code executed when checking key movement:
    Public Sub selectHelpOrChoosePlayer()

        canSelect = False
        helpVis = True
        buttonTexts(2) = "Back"
        buttCount = 2

        For i = 0 To 2
            buttonDown(i) = False
        Next

    End Sub

    'Common bit of code executed when checking key movement:
    Public Sub moveUpOrDown(UpTrue As Boolean, UpFalse As Boolean, ID As Integer) 'booleans are *true *when moving buttons Up
        '                                                                           ID is 0 when going up, 2 when going down

        canSelect = False
        If backSelected = UpTrue And choosingChar = True Then
            backSelected = UpFalse
            buttonDown(2) = UpFalse
            headSelected(0) = True
            If UpTrue = True Then
                headCount = 0
            Else
                For i = 0 To 2
                    headSelected(i) = False
                Next
            End If
        ElseIf choosingChar = False And buttonDown(ID) = False And helpVis = False Then
            buttonDown(buttCount) = False
            If UpTrue = True Then
                buttCount -= 1
            Else
                buttCount += 1
            End If
            buttonDown(buttCount) = True
        End If

    End Sub

    'Common bit of code executed when checking key movement:
    Public Sub moveRightOrLeft(RightTrue As Boolean) 'boolean is *true *when moving heads moving right

        canSelect = False
        headSelected(headCount) = False
        If RightTrue = True Then
            headCount += 1
        Else
            headCount -= 1
        End If

        headSelected(headCount) = True

    End Sub

    Public Sub displayMsgBox(spritebatch As SpriteBatch) 'draw the message box and its text

        spritebatch.Draw(MsgBox, New Rectangle(243, 147, 320, 180), Color.White)

        If choosingChar = True Then
            spritebatch.DrawString(msgfont, "Choose A Character", New Vector2(300, 150), Color.Black)
            For i = 0 To 2
                headArray(i).draw(spritebatch, headSelected(i))
                If headSelected(i) = True Then 'display character text
                    spritebatch.DrawString(msgfont, headText1(i), New Vector2(280, 278), Color.Black)
                    spritebatch.DrawString(msgfont, headText2(i), New Vector2(300, 298), Color.Black)
                End If
            Next
        Else 'display help information
            spritebatch.Draw(helpText, New Rectangle(245, 147, 315, 182), Color.White)
        End If

    End Sub

    Public Sub draw(spriteBatch As SpriteBatch) 'render menu items

        spriteBatch.Draw(MenuTexture, New Rectangle(0, 0, 800, 480), Color.White)

        For i = 0 To 2 'display all buttons
            buttonArray(i).draw(spriteBatch)
            spriteBatch.DrawString(Buttonfont, buttonTexts(i), New Vector2(364, (buttonPos(i) + 15)), Color.White)
        Next

        If helpVis = True Then 'If in help screen or choosing player
            displayMsgBox(spriteBatch)
        End If

    End Sub

End Class
